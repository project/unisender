<?php

/**
 * @file
 * Unisender module.
 */

define('UNISENDER_QUEUE_CRON', 'unisender');
define('UNISENDER_BATCH_QUEUE_CRON', 'unisender_batch');

define('UNISENDER_STATUS_SENT', 'sent');
define('UNISENDER_STATUS_SAVE', 'save');
define('UNISENDER_STATUS_PAUSED', 'paused');
define('UNISENDER_STATUS_SCHEDULE', 'schedule');
define('UNISENDER_STATUS_SENDING', 'sending');

/**
 * Implements hook_menu().
 */
function unisender_menu() {
  $items = array();

  $items['admin/config/services/unisender'] = array(
    'title' => 'Unisender',
    'description' => 'Manage Unisender Settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('variable_group_form', 'unisender_main_settings'),
    'access arguments' => array('unisender main settings'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/services/unisender/global'] = array(
    'title' => 'Global Settings',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['unisender/webhook'] = array(
    'title' => 'Unisender webhooks endpoint',
    'page callback' => 'unisender_process_webhook',
    'access callback' => 'unisender_process_webhook_access',
    'access arguments' => array(2),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function unisender_permission() {
  $permissions = array();

  $permissions['administer unisender'] = array(
    'title' => t('administer unisnender'),
    'description' => t('Access the unisender configuration options.'),
  );
  $permissions['unisender main settings'] = array(
    'title' => t('Unisnender main setttings'),
    'description' => t('Access the unisender main options.'),
  );

  return $permissions;
}

/**
 * Implements hook_libraries_info().
 */
function unisender_libraries_info() {
  $libraries = array();
  
  $libraries['unisender'] = array(
    'name' => 'Unisender API',
    'vendor url' => 'https://github.com/unisender-dev/php-api-wrapper/',
    'download url' => 'https://github.com/unisender-dev/php-api-wrapper/archive/master.zip',
    'version arguments' => array(
      'file' => 'unisenderApi.php',
      'pattern' => '/@version ([0-9\.]+)/',
    ),
    'files' => array(
      'php' => array(
        'unisenderApi.php',
      ),
    ),
  );

  return $libraries;
}


/**
 * Access callback for unisender submodule menu items.
 */
function unisender_apikey_ready_access($permission) {
  return unisender_get_api() && user_access($permission);
}

/**
 * Get an instance of the Unisender library.
 *
 * @return unisenderApi
 *   Instance of the Unisender library class. Can be overridden by $classname.
 */
function unisender_get_api() {
  $unisender = &drupal_static(__FUNCTION__);
  if (isset($unisender) && $unisender instanceof unisenderApi) {
    return $unisender;
  }
  
  if (module_exists('libraries')) {
    $library = libraries_load('unisender');
  }
  else {
    $library = FALSE;
  }
  if (!$library['installed'] && !class_exists('unisenderApi')) {
    $msg = t('Failed to load Unisender PHP library. Please refer to the installation requirements.');
    watchdog('unisender', $msg, array(), WATCHDOG_ERROR);
    drupal_set_message($msg, 'error', FALSE);
    return NULL;
  }

  $api_key = variable_get_value('unisender_api_key');

  if (empty($api_key)) {
    watchdog('unisender', 'Unisender Error: API Key cannot be blank.', array(), WATCHDOG_ERROR);
    return NULL;
  }

  $timeout = variable_get_value('unisender_api_timeout');
  $is_compression = variable_get_value('unisender_api_compression');

  return new unisenderApiWrapper($api_key, 'UTF-8', 4, $timeout, $is_compression);
}

/**
 * Wrapper around unisender_get_lists() to return a single list.
 *
 * @param string $list_id
 *   The unique ID of the list provided by Unisender.
 *
 * @return array
 *   A list array formatted as indicated in the Unisender API documentation.
 */
function unisender_get_list($list_id) {
  $lists = unisender_get_lists();
  return $lists[$list_id] ?? [];
}

/**
 * Return all Unisender lists. Lists are stored in the cache.
 *
 * @param bool $reset
 *   Force a cache reset.
 *
 * @return array
 *   An array of list arrays.
 */
function unisender_get_lists($reset = FALSE) {
  $cache = $reset ? NULL : cache_get('lists', 'cache_unisender');
  $lists = array();
  // Return cached lists:
  if ($cache) {
    $lists = $cache->data;
  }
  // Query lists from the Unisender API and store in cache:
  else {
    $unisender = unisender_get_api();
    if (!$unisender) {
      return array();
    }

    $result = $unisender->getLists();
    if (!$result || isset($result['error'])) {
      return $result;
    }

    foreach ($result as $list) {
      $contact_count = $unisender->getContactCount(array(
        'list_id' => $list['id'],
        'params' => ['type' => 'address'],
      ));
      $list['members_count'] = $contact_count['count'] ?? 0;
      $lists[$list['id']] = $list;
    }

    cache_set('lists', $lists, 'cache_unisender', CACHE_TEMPORARY);
  }

  return $lists;
}

/**
 * Wrapper around Unisender->getFields().
 *
 * @param bool $reset
 *   Set to TRUE if fields should not be loaded from cache.
 *
 * @return array
 *   Struct describing field.
 */
function unisender_get_fields($reset = FALSE) {
  if (!$reset) {
    $cache = cache_get('fields', 'cache_unisender');
    // Get cached data and unset from our remaining lists to query.
    if ($cache) {
      return $cache->data;
    }
  }

  $unisender = unisender_get_api();

  try {
    if (!$unisender) {
      throw new Exception('Cannot get fields without Unisender API. Check API key has been entered.');
    }

      // Add default email field.
    $fields = array(
      array(
        'name' => 'email',
        'public_name' => t('Email Address'),
        'type' => 'string',
        'required' => TRUE,
        'is_visible' => TRUE,
        'view_pos' => 1,
      ),
    );

    $result = $unisender->getFields();

    if (!isset($result['error']) && count($result)) {
      $fields = array_merge($fields, $result);
    }

    cache_set('fields', $fields, 'cache_unisender', CACHE_TEMPORARY);
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred requesting fields. "%message"', array(
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
  }

  return $fields;
}

/**
 * Wrapper around Unisender->getTags().
 *
 * @param bool $reset
 *   Set to TRUE if fields should not be loaded from cache.
 *
 * @return array
 *   Struct describing tag.
 */
function unisender_get_tags($reset = FALSE) {
  if (!$reset) {
    $cache = cache_get('tags', 'cache_unisender');
    // Get cached data and unset from our remaining lists to query.
    if ($cache) {
      return $cache->data;
    }
  }

  $unisender = unisender_get_api();

  try {
    if (!$unisender) {
      throw new Exception('Cannot get tags without Unisender API. Check API key has been entered.');
    }

    $result = $unisender->getTags();

    if (!isset($result['error']) && count($result)) {
      $tags = array_column($result, 'name', 'id');
    }

    cache_set('tags', $tags, 'cache_unisender', CACHE_TEMPORARY);
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred requesting tags. "%message"', array(
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
  }

  return $tags;
}

/**
 * Get the Unisender member info for a given email address and list.
 *
 * Results are cached in the cache_unisender bin which is cleared by the
 * Unisender web hooks system when needed.
 *
 * @param string $list_id
 *   The Unisender list ID to get member info for.
 * @param string $email
 *   The Unisender user email address to load member info for.
 * @param bool $reset
 *   Set to TRUE if member info should not be loaded from cache.
 *
 * @return object
 *   Member info object, empty if there is no valid info.
 */
function unisender_get_memberinfo($list_id, $email, $reset = FALSE) {
  $cache = $reset ? NULL : cache_get($list_id . '-' . $email, 'cache_unisender');

  // Return cached lists:
  if ($cache) {
    return $cache->data;
  }

  // Query lists from the API and store in cache:
  $memberinfo = array();

  $unisender = unisender_get_api();
  try {
    if (!$unisender) {
      throw new Exception('Cannot get member info without Unisender API. Check API key has been entered.');
    }

    try {
      $result = $unisender->getContact(array(
        'email' => $email,
        'include_lists' => 1,
        'include_fields' => 1,
        'include_details' => 1,
      ));

      if (!isset($result['error']) && !empty($result['lists'])) {
        $lists_ids = array_column($result['lists'], 'status', 'id');
        if (isset($lists_ids[$list_id]) && $lists_ids[$list_id] === 'active') {
          $memberinfo = array_merge($result['fields'], $result['email']);
          cache_set($list_id . '-' . $email, $memberinfo, 'cache_unisender', CACHE_TEMPORARY);
        }
      }
    }
    catch (Exception $e) {
      // Throw exception only for errors other than member not found.
      if ($e->getCode() != 404) {
        throw new Exception($e->getMessage(), $e->getCode(), $e);
      }
    }
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred requesting memberinfo for @email in list @list. "%message"', array(
      '@email' => $email,
      '@list' => $list_id,
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
  }

  return $memberinfo;
}

/**
 * Check if the given email is subscribed to the given list.
 *
 * Simple wrapper around unisender_get_memberinfo().
 *
 * @param string $list_id
 *   Unique string identifier for the list on your Unisender account.
 * @param string $email
 *   Email address to check for on the identified Unisender List.
 * @param bool $reset
 *   Set to TRUE to ignore the cache. (Used heavily in testing functions.)
 *
 * @return bool
 *   Indicates subscription status.
 */
function unisender_is_subscribed($list_id, $email, $reset = FALSE) {
  $memberinfo = unisender_get_memberinfo($list_id, $email, $reset);

  return isset($memberinfo['status']) && $memberinfo['status'] === 'active';
}

/**
 * Subscribe a user to a Unisender list in real time or by adding to the queue.
 *
 * @see Unisender::subscribe()
 *
 * @return bool
 *   True on success.
 */
function unisender_subscribe($list_id, $email, $fields = NULL, $tags = array(), $double_optin = FALSE) {
  if (variable_get('unisender_cron', FALSE)) {
    $args = array(
      'list_id' => $list_id,
      'email' => $email,
      'fields' => $fields,
      'tags' => $tags,
      'double_optin' => $double_optin,
    );
    return unisender_addto_queue('unisender_subscribe_process', $args);
  }

  return unisender_subscribe_process($list_id, $email, $fields, $tags, $double_optin);
}

/**
 * @return object
 *   On success a result object will be returned from Unisender. On failure an
 *   object will be returned with the property success set to FALSE, the
 *   response code as a property, and the message as a property. To check for
 *   a failure, look for the property 'success' of the object returned to
 *   be set to FALSE.
 */
function unisender_subscribe_process($list_id, $email, $fields = NULL, $tags = array(), $double_optin = FALSE) {
  $result = FALSE;
  try {
    $unisender = unisender_get_api();
    if (!$unisender) {
      throw new Exception('Cannot subscribe to list without Unisender API. Check API key has been entered.');
    }

    $parameters = array(
      'list_ids' => $list_id,
      // If double opt-in is required, set member status to 'pending'.
      'double_optin' => $double_optin ? 0 : 3,
    );

    // Set tags.
    if (!empty($tags)) {
      $parameters['tags'] = implode(',', $tags);
    }

    $fields[UNISENDER_FIELD_EMAIL] = $email;
    // Set fields.
    if (!empty($fields)) {
      foreach ($fields as $key => $value) {
        $parameters["fields[{$key}]"] = $value;
      }
    }

    // Add member to list.
    $result = $unisender->subscribe($parameters);

    if (!isset($result['error'])) {
      // Clear user cache, just in case there's some cruft leftover:
      unisender_cache_clear_member($list_id, $email);

      module_invoke_all('unisender_subscribe_user', $list_id, $email, $fields);

      watchdog('unisender', '@email was subscribed to list @list.',
        array('@email' => $email, '@list' => $list_id), WATCHDOG_NOTICE
      );
    }
    else {
      if (!variable_get('unisender_test_mode')) {
        watchdog('unisender', 'A problem occurred subscribing @email to list @list.'.print_r($result, TRUE), array(
          '@email' => $email,
          '@list' => $list_id,
        ), WATCHDOG_WARNING);
      }
    }
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred subscribing @email to list @list. Status code @code. "%message"', array(
      '@email' => $email,
      '@list' => $list_id,
      '%message' => $e->getMessage(),
      '@code' => $e->getCode(),
    ), WATCHDOG_ERROR);
    $result = new stdClass();
    $result->success = FALSE;
    $result->status = $e->getCode();
    $result->message = $e->getMessage();
  }
  finally {
    return $result;
  }
}

/**
 * Add a Unisender subscription task to the queue.
 *
 * @string $function
 *   The name of the function the queue runner should call.
 * @array $args
 *   The list of args to pass to the function.
 *
 * @return bool
 *   Success or failure.
 */
function unisender_addto_queue($function, $args) {
  $queue = DrupalQueue::get(UNISENDER_QUEUE_CRON);
  $queue->createQueue();
  unisender_update_local_cache($function, $args);
  return $queue->createItem(array(
    'function' => $function,
    'args' => $args,
  ));
}

/**
 * Updates the local cache for a user as though a queued request had been processed.
 *
 * If we don't do this, then users can make changes, but not have them shown on the site until
 * cron runs, which is intensely confusing. See https://www.drupal.org/node/2503597
 *
 * @string $function
 *   The name of the function that the queue runner will call when the update is processed.
 * @array $args
 *   The list of args that will be passed to the queue runner.
 */
function unisender_update_local_cache($function, $args) {
  $list_id = isset($args['list_id']) ? $args['list_id'] : NULL;
  $email   = isset($args['email']) ? $args['email'] : NULL;
  if (empty($list_id) || empty($email)) {
    return FALSE;
  }
  $cache = unisender_get_memberinfo($list_id, $email);
  if (empty($cache)) {
    // Create a new entry.
    cache_set($list_id . '-' . $email,
      array('fields' => array()),
      'cache_unisender',
      CACHE_TEMPORARY);
    $cache = cache_get($list_id . '-' . $email, 'cache_unisender');
    $cache = $cache->data;
  }
  // Handle unsubscribes.
  if ($function == 'unisender_unsubscribe_process') {
    $cache['status'] = 'unsubscribed';
    // Reset tags.
    $cache['tags'] = array();
  }
  // Handle subscribes.
  if ($function == 'unisender_subscribe_process') {
    $cache['status'] = 'subscribed';
  }
  // Handle member updates.
  if ($function == 'unisender_update_member_process' ||
      $function == 'unisender_subscribe_process') {
    // Update cached fields.
    if (!isset($cache['fields'])) {
      $cache['fields'] = array();
    }
    foreach ($args['fields'] as $key => $value) {
      $cache['fields'][$key] = $value;
    }
    // Update cached tags.
    $cache['tags'] = array();
    foreach ($args['tags'] as $id => $name) {
      if ($name !== 0) {
        $cache['tags'][$id] = TRUE;
      }
    }
  }
  // Store the data back in the local cache.
  cache_set($list_id . '-' . $email, $cache, 'cache_unisender', CACHE_TEMPORARY);
}

/**
 * Update a members list subscription in real time or by adding to the queue.
 *
 * @return bool
 *   Success or failure.
 */
function unisender_update_member($list_id, $email, $fields, $tags, $format = 'html') {
  if (variable_get('unisender_cron', FALSE)) {
    $args = array(
      'list_id' => $list_id,
      'email' => $email,
      'fields' => $fields,
      'tags' => $tags,
      'format' => $format,
    );
    return unisender_addto_queue('unisender_update_member_process', $args);
  }

  return unisender_update_member_process($list_id, $email, $fields, $tags, $format);
}

/**
 * Wrapper around Unisender::updateMember().
 *
 * @return bool
 *   Success or failure.
 */
function unisender_update_member_process($list_id, $email, $fields, $tags) {
  $result = FALSE;
  try {
    $unisender = unisender_get_api();
    if (!$unisender) {
      throw new Exception('Cannot update member without Unisender API. Check API key has been entered.');
    }

    if (isset($fields['email']) && ($email != $fields['email'])) {
      $result = unisender_unsubscribe_process($list_id, $email);
      $email = $fields['email'];
      $result = unisender_subscribe_process($list_id, $email, $fields, $tags);
    }
    else {
      // Update member.
      $result = unisender_subscribe_process($list_id, $email, $fields, $tags);
    }

    if (!empty($result) && isset($result->id)) {
      watchdog('unisender', '@email was updated in list @list_id.', array(
        '@email' => $email,
        '@list' => $list_id,
      ), WATCHDOG_NOTICE);
      // Clear user cache:
      unisender_cache_clear_member($list_id, $email);
    }
    else {
      watchdog('unisender', 'A problem occurred updating @email on list @list.', array(
        '@email' => $email,
        '@list' => $list_id,
      ), WATCHDOG_WARNING);
    }
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred updating @email on list @list. "%message"', array(
      '@email' => $email,
      '@list' => $list_id,
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
  }
  return $result;
}

/**
 * Retrieve all members of a given list with a given status.
 *
 * Note that this function can cause locking an is somewhat slow. It is not
 * recommended unless you know what you are doing! See API documentation.
 */
function unisender_get_members($list_id, $status = 'subscribed', $options = array()) {
  $results = FALSE;
  if (lock_acquire('unisender_get_members', 60)) {
    try {
      $unisender = unisender_get_api();
      if (!$unisender) {
        throw new Exception('Cannot get members without Unisender API. Check API key has been entered.');
      }

      $options['status'] = $status;
      $options['count'] = 500;

      $results = $unisender->getMembers($list_id, $options);
    }
    catch (Exception $e) {
      watchdog('unisender', 'An error occurred pulling member info for a list. "%message"', array(
        '%message' => $e->getMessage(),
      ), WATCHDOG_ERROR);
    }
    lock_release('unisender_get_members');
  }
  return $results;
}

/**
 * Wrapper around Unisender->addOrUpdateMember().
 *
 * $batch is an array where each element is an array formatted thus:
 *   'email' => 'example@example.com',
 *   'fields' => array('MERGEKEY' => 'value', 'MERGEKEY2' => 'value2'),
 */
function unisender_batch_update_members($list_id, $batch, $double_in = FALSE) {
  try {
    $unisender = unisender_get_api();
    if (!$unisender) {
      throw new Exception('Cannot batch subscribe to list without Unisender API. Check API key has been entered.');
    }

    if (!empty($batch)) {
      // Create a new batch update operation for each member.
      foreach ($batch as $batch_data) {
        // TODO: Remove 'advanced' earlier? Needed at all?
        unset($batch_data['fields']['advanced']);

        $parameters = array(
          'fields' => (object) $batch_data['fields'],
        );

        $unisender->addOrUpdateMember($list_id, $batch_data['email'], $parameters, TRUE);
      }

      // Process batch operations.
      return $unisender->processBatchOperations();
    }
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred performing batch subscribe/update. "%message"', array(
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
  }
}

/**
 * Unsubscribe a member from a list.
 *
 * @param string $list_id
 *   A unisender list id.
 * @param string $email
 *   Email address to be unsubscribed.
 * @param bool $delete
 *   Indicates whether an email should be deleted or just unsubscribed.
 * @param bool $goodbye
 *   Indicates whether to send the goodbye email to the email address.
 * @param bool $notify
 *   Indicates whether to send the unsubscribe notification email to the address
 *   defined in the list email notification settings.
 *
 * @return bool
 *   Indicates whether unsubscribe was successful.
 */
function unisender_unsubscribe($list_id, $email, $delete = FALSE, $goodbye = FALSE, $notify = FALSE) {
  $result = FALSE;

  if (unisender_is_subscribed($list_id, $email)) {
    if (variable_get('unisender_cron', FALSE)) {
      $result = unisender_addto_queue(
        'unisender_unsubscribe_process',
        array(
          'list_id' => $list_id,
          'email' => $email,
          'delete' => $delete,
          'goodbye' => $goodbye,
          'notify' => $notify,
        )
      );
    }
    else {
      $result = unisender_unsubscribe_process($list_id, $email, $delete, $goodbye, $notify);
    }
  }

  return $result;
}

/**
 * Wrapper around Unisender::unsubscribe().
 *
 * @see Unisender::unsubscribe()
 *
 * @return bool
 *   Success or failure.
 */
function unisender_unsubscribe_process($list_id, $email) {
  try {
    $unisender = unisender_get_api();
    if (!$unisender) {
      throw new Exception('Cannot unsubscribe from list without Unisender API. Check API key has been entered.');
    }

    $result = $unisender->unsubscribe(array(
      'list_ids' => $list_id,
      'contact_type' => 'email',
      'contact' => $email,
    ));


    module_invoke_all('unisender_unsubscribe_user', $list_id, $email);

    // Clear user cache:
    unisender_cache_clear_member($list_id, $email);

    return TRUE;
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred unsubscribing @email from list @list. "%message"', array(
      '@email' => $email,
      '@list' => $list_id,
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);

    return FALSE;
  }
}

/**
 * Wrapper around Unisender->getContact()`.
 *
 * Returns all lists a given email address is currently subscribed to.
 *
 * @param string $email
 *   Email address to search.
 *
 * @return array
 *   structs containing id, name.
 */
function unisender_get_lists_for_email($email) {
  try {
    $unisender = unisender_get_api();

    if (!$unisender) {
      throw new Exception('Cannot get lists without Unisender API. Check API key has been entered.');
    }

    $result = $unisender->getContact(array(
      'email' => $email,
      'include_lists' => 1,
      'include_fields' => 0,
      'include_details' => 0,
    ));
    $lists = $result['lists'] ?? [];
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred retreiving lists data for @email. "%message"', array(
      '@email' => $email,
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
    $lists = array();
  }
  return $lists;
}

/**
 * Wrapper around Unisender->listHooks().
 *
 * @return mixed
 *   Array of existing webhooks, or FALSE.
 */
function unisender_unsubscribe_webhook_get() {
  try {
    $unisender = unisender_get_api();
    if (!$unisender) {
      throw new Exception('Cannot get webhook without Unisender API. Check API key has been entered.');
    }

    $result = $unisender->listHooks();

    return $result;
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred reading webhooks for list @list. "%message"', array(
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Wrapper around Unisender->setHook().
 *
 * @return mixed
 *   New webhook ID if added, FALSE otherwise.
 */
function unisender_unsubscribe_webhook_add($url) {
  try {
    $unisender = unisender_get_api();
    if (!$unisender) {
      throw new Exception('Cannot add webhook without Unisender API. Check API key has been entered.');
    }

    $result = $unisender->setHook(array(
      'hook_url' => $url,
      "events[unsubscribe]" => '*',
      'event_format' => 'json_post_gzip',
    ));

    return isset($result['success']);
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred adding unsubscribe webhook. "%message"', array(
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Wrapper around Unisender->removeHook().
 *
 * @return bool
 *   TRUE if deletion was successful, otherwise FALSE.
 */
function unisender_unsubscribe_webhook_delete($url) {
  try {
    $unisender = unisender_get_api();
    if (!$unisender) {
      throw new Exception('Cannot delete webhook without Unisender API. Check API key has been entered.');
    }

    $result = $unisender->removeHook(array(
      'hook_url' => $url
    ));

    return isset($result['status']) && $result['status'] === 'success';
  }
  catch (Exception $e) {
    watchdog('unisender', 'An error occurred deleting unsubscribe webhook. "%message"', array(
      '%message' => $e->getMessage(),
    ), WATCHDOG_ERROR);
    return FALSE;
  }
}

/**
 * Clear a unisender user memberinfo cache.
 *
 * @string $list_id
 * @string $email
 */
function unisender_cache_clear_member($list_id, $email) {
  cache_clear_all($list_id . '-' . $email, 'cache_unisender');
}

/**
 * Clear all unisender caches.
 */
function unisender_cache_clear_all() {
  cache_clear_all('*', 'cache_unisender', TRUE);
}

/**
 * Implements hook_flush_caches().
 */
function unisender_flush_caches() {
  return array('cache_unisender');
}

/**
 * Access callback for unisender_process_webhook().
 *
 * @string $key
 */
function unisender_process_webhook_access($key) {
  return $key == unisender_webhook_key();
}

/**
 * Process a webhook post from Unisender.
 */
function unisender_process_webhook() {
  if (empty($_POST)) {
    return "Unisender Webhook Endpoint.";
  }
  
  $post_data = file_get_contents('php://input');
  if (strlen($post_data) > 0) {
    $decoded_data = json_decode(gzdecode($post_data), TRUE);
  }
  if (!unisender_check_webhook_data($decoded_data)) {
    watchdog('unisender', 'Webhook data signed with incorrect hash!' . print_r($decoded_data, TRUE), WATCHDOG_ERROR);
    return;
  }
  
//  $data = $_POST['data'];
//  $type = $_POST['type'];
//  switch ($type) {
//    case 'unsubscribe':
//    case 'profile':
//    case 'cleaned':
//      unisender_get_memberinfo($data['list_id'], $data['email'], TRUE);
//      break;
//
//    case 'upemail':
//      unisender_cache_clear_member($data['list_id'], $data['old_email']);
//      unisender_get_memberinfo($data['list_id'], $data['new_email'], TRUE);
//      break;
//
//    case 'campaign':
//      unisender_cache_clear_list_activity($data['list_id']);
//      unisender_cache_clear_campaign($data['id']);
//      break;
//  }
//
//  // Allow other modules to act on a webhook.
//  module_invoke_all('unisender_process_webhook', $type, $data);

  // Log event:
  watchdog('unisender', 'Webhook has been processed: ' . print_r($decoded_data, TRUE), WATCHDOG_ERROR);
  
  return NULL;
}

function unisender_check_webhook_data($decoded_data) {
  $received_hash = $decoded_data['auth'];
  $decoded_data['auth'] = variable_get_value('unisender_api_key');
  return md5(json_encode($decoded_data)) === $received_hash;
}

/**
 * Generate a key to include in the webhook url based on a hash.
 *
 * @string $list_id
 *
 * @return string
 *   The key.
 */
function unisender_webhook_key() {
  return drupal_hash_base64($GLOBALS['base_url'] . drupal_get_private_key() . drupal_get_hash_salt());
}

/**
 * Generate the webhook endpoint URL.
 *
 * @string $list_id
 *
 * @return string
 *   The endpoint URL.
 */
function unisender_webhook_url() {
  return $GLOBALS['base_url'] . '/unisender/webhook/' . unisender_webhook_key();
}

/**
 * Helper function to generate form elements for a list's interest groups.
 *
 * @param array $list
 *   Fully loaded array with unisender list settings as returned by
 *   unisender_get_list()
 * @param array $defaults
 *   Array of default values to use if no group subscription values already
 *   exist at Unisender.
 * @param string $email
 *   Optional email address to pass to the MCAPI and retrieve existing values
 *   for use as defaults.
 *
 * @return array
 *   A collection of form elements, one per interest group.
 */
function unisender_tags_form_elements($list = NULL, $defaults = array(), $email = NULL) {
  $return = array();

  $unisender = unisender_get_api();

  $tags = $unisender->getTags();

  if (!empty($email)) {
    $memberinfo = unisender_get_memberinfo($list['id'], $email);
  }

  // Extract the field options:
  $options = array_column($tags, 'name', 'id');

//  $default_values = array();
    // Set interest options and default values.
//    foreach ($tags as $tag) {
//      if (isset($memberinfo)) {
//        if (isset($memberinfo->interests->{$interest->id}) && ($memberinfo->interests->{$interest->id} === TRUE)) {
//          $default_values[$group->id][] = $interest->id;
//        }
//      }
//      elseif (!empty($defaults)) {
//        if (isset($defaults[$group->id][$interest->id]) && !empty($defaults[$group->id][$interest->id])) {
//          $default_values[$group->id][] = $interest->id;
//        }
//      }
//    }

  $return = array(
    '#type' => 'checkboxes',
//    '#title' => $group->title,
    '#options' => $options,
//    '#default_value' => isset($default_values[$group->id]) ? $default_values[$group->id] : array(),
    '#attributes' => array('class' => array('unisender-newsletter-tags')),
  );

  return $return;
}

/**
 * Convert unisender form elements to Drupal Form API.
 *
 * @param array $field
 *   The unisender-formatted form element to convert.
 *
 * @return array
 *   A properly formatted drupal form element.
 */
function unisender_insert_drupal_form_tag($field) {
  // Insert common FormAPI properties:
  $input = array(
    '#title' => t('@field', array('@field' => $field['public_name'])),
    '#weight' => $field['view_pos'],
    '#required' => isset($field['required']) && $field['required'],
  );

  switch ($field['type']) {
    case 'bool':
      $input['#type'] = 'checkbox';
      break;

    default:
      $input['#type'] = 'textfield';
      break;
  }

  // Special cases for Unisender hidden defined fields:
  if ($field['is_visible'] === FALSE) {
    $input['#type'] = 'hidden';
  }

  return $input;
}

/**
 * Implements hook_cron().
 *
 * We don't use batch API calls currently as it would require sorting through
 * a lot of options here. Instead, we will provide VBO functions to perform
 * large unsubscribes and subscribes and specifically call the batch functions.
 */
function unisender_cron() {
  $queue = DrupalQueue::get(UNISENDER_QUEUE_CRON);
  $queue->createQueue();
  $queue_count = $queue->numberOfItems();
  if ($queue_count > 0) {
    $batch_limit = variable_get('unisender_batch_limit', 100);
    $batch_size = ($queue_count < $batch_limit) ? $queue_count : $batch_limit;
    $count = 0;
    while ($count < $batch_size) {
      if ($item = $queue->claimItem()) {
        call_user_func_array($item->data['function'], $item->data['args']);
        $queue->deleteItem($item);
      }
      $count++;
    }
  }
}

/**
 * Replace an array of merge field placeholder tokens with their values.
 *
 * @param array $fields
 *   An array of merge fields and token place holders to be expanded. The key
 *   of the array is the Unisender field name, the value is the token that
 *   should be expanded.
 * @param object $entity
 *   The entity that contains the values to use for token replacement.
 * @param string $entity_type
 *   The entity type of the entity being used.
 *
 * @return mixed
 *   Associative array of Unisender fields with values taken from the provided
 *   entity.
 */
function unisender_fields_populate($fields, $entity, $entity_type) {
  $uni_fields = drupal_static(__FUNCTION__, array());
  list($id, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  if (!isset($uni_fields[$bundle . ':' . $id])) {
    foreach ($fields as $key => $token) {
      $uni_fields[$bundle . ':' . $id][$key] = token_replace($token, array($entity_type => $entity), array('clear' => TRUE));
    }
  }

  return $uni_fields[$bundle . ':' . $id];
}


function unisender_form_variable_group_form_alter(&$form) {
  if (isset($form['#variable_group_form']) && $form['#variable_group_form'] === 'unisender_main_settings') {
    $form['#submit'][] = '_unisender_variable_group_connection_settings_submit';
  }
  
  if (!function_exists('bzcompress')) {
    $form['unisender_api_compression']['#value'] = FALSE;
    $form['unisender_api_compression']['#disabled'] = TRUE;
    $form['unisender_api_compression']['#description'] = t('bzip2 module for PHP is needed to enable this option.');
  }
}

function _unisender_variable_group_connection_settings_submit($form, &$form_state) {
  $required_variables = array('unisender_api_timeout', 'unisender_api_key');
  foreach ($required_variables as $key) {
    if (!isset($form_state['values'][$key]) || empty($form_state['values'][$key])) {
      return;
    }
  }
  $result = unisender_get_lists(TRUE);
  if (is_array($result) && !isset($result['error'])) {
    drupal_set_message(t('Unisender API connection checked: success!'));
  }
  else {
    drupal_set_message(t('Error while checking API connection! @message', array('@message' => $result['error'])), 'warning');
  }
}