<?php

function unisender_variable_group_info() {
  $groups = array();

  $groups['unisender_mail_settings'] = array(
    'title' => t('Main settings'),
    'access' => 'unisender main settings',
  );

  return $groups;
}

function unisender_variable_info() {
  $variables = array();

  $variables['unisender_api_key'] = array(
    'title' => t('Server API Key'),
    'group' => 'unisender_main_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 40,
    ),
    'token' => TRUE,
    'access' => 'unisender main settings',
  );
  $variables['unisender_api_timeout'] = array(
    'title' => t('main timeout'),
    'type' => 'number',
    'group' => 'unisender_main_settings',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 3,
      '#field_suffix' => t('sec'),
    ),
    'default' => 60,
    'token' => TRUE,
    'access' => 'unisender main settings',
  );
  $variables['unisender_api_compression'] = array(
    'title' => t('Compression'),
    'type' => 'boolean',
    'group' => 'unisender_main_settings',
    'default' => FALSE,
    'token' => TRUE,
    'access' => 'unisender main settings',
  );

  $variables['unisender_cron'] = array(
    'title' => t('Use batch subscribes processing on cron'),
    'type' => 'boolean',
    'group' => 'unisender_main_settings',
    'default' => FALSE,
    'access' => 'unisender main settings',
  );
  $variables['unisender_batch_limit'] = array(
    'title' => t('Batch limit'),
    'type' => 'number',
    'group' => 'unisender_main_settings',
    'default' => 50,
    'access' => 'unisender main settings',
  );

  return $variables;
}
