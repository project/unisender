<?php

/**
 * @file
 * Rules integration for the checkout process.
 *
 * @addtogroup rules
 * @{
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_unisender_rules_action_info() {
  $items = array();

  $items['commerce_unisender_user_subscribe_order'] = array(
    'label' => t('Subscribe user to unisender lists of an order'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order in checkout'),
      ),
    ),
    'group' => t('Unisender'),
    'access callback' => 'unisender_lists_rules_access_callback',
    'base' => 'commerce_unisender_rules_action_user_subscribe_order',
  );

  $items['commerce_unisender_lists_email_subscribe_list'] = array(
    'label' => t('Subscribe order email address to a specific unisender list'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
      'list' => array(
        'type' => 'text',
        'label' => t('Unisender List'),
        'options list' => 'commerce_unisender_lists_groups_options_list',
      ),
    ),
    'group' => t('Unisender'),
    'access callback' => 'unisender_lists_rules_access_callback',
    'base' => 'commerce_unisender_lists_rules_action_email_subscribe_list',
  );

  return $items;
}

/**
 * Unisender Lists options callback
 */
function commerce_unisender_lists_groups_options_list() {
  $lists = unisender_get_lists();
  return array_column($lists, 'title', 'id');
}

/**
 * Action callback: Subscribe a user to lists of an order.
 */
function commerce_unisender_rules_action_user_subscribe_order($order) {
  if (!isset($order->data['commerce_unisender'])) {
    return;
  }
  
  $fields = unisender_get_fields();
  foreach ($order->data['commerce_unisender'] as $list_id => $value) {
    if ($value) {
      $order_fields = $fields ? token_generate('commerce-order', $fields, array('commerce-order' => $order)) : array();
      $tags = isset($order->data['commerce_unisender_tags']) ? $order->data['commerce_unisender_tags'] : array();
      unisender_subscribe($list_id, $order->mail, $order_fields, $tags);
    }
  }
}

/**
 * Action callback: Subscribe an email address to a list.
 */
function commerce_unisender_lists_rules_action_email_subscribe_list($order, $list_id) {
  $fields = unisender_get_fields();
  $order_fields = $fields ? token_generate('commerce-order', $fields, array('commerce-order' => $order)) : array();

  unisender_subscribe($list_id, $order->mail, $order_fields);
}

/**
 * @}
 */
