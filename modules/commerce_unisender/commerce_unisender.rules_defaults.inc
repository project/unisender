<?php

/**
 * @file
 * Default rule configurations for Checkout.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_unisender_default_rules_configuration() {

  $rules = array();

  // Add a reaction rule to subscribe user to unisender lists of an order
  // upon checkout completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Subscribe user to unisender lists of an order on checkout completion');
  $rule->active = TRUE;

  $rule
    ->event('commerce_checkout_complete')
    ->action('commerce_unisender_user_subscribe_order', array(
      'commerce_order:select' => 'commerce-order',
    ));

  $rule->weight = 50;
  $rules['commerce_checkout_commerce_unisender_user_subscribe_order'] = $rule;

  return $rules;
}
