<?php

/**
 * @file
 * Class file for Unisender Signup UI Controller.
 */

/**
 * Override EntityDefaultUIController to customize our menu items.
 */
class UnisenderSignupUIController extends EntityDefaultUIController {

  /**
   * Overrides parent::hook_menu().
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['title'] = t('Signup Forms');
    $items[$this->path]['description'] = t('Manage Unisender Signup blocks.');
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    $items[$this->path]['weight'] = 10;
    $items[$this->path]['access callback'] = 'unisender_signup_entity_access';

    return $items;
  }

  /**
   * Overrides parent::overviewTable().
   */
  public function overviewTable($conditions = array()) {
    $render = parent::overviewTable($conditions);
    $lists = unisender_get_lists();
    foreach ($render['#rows'] as &$row) {
      $signup = $row[0]['data']['#url']['options']['entity'];

      $list_labels = array();
      foreach ($signup->lists as $list_id) {
        $list_labels[] = l($lists[$list_id]['title'], 'https://cp.unisender.com/en/v5/lists');
      }
      $new_row = array();
      // Put the label column data first:
      $new_row[] = array_shift($row);
      // Now our custom columns:
      $new_row[] = l(t('Block'), 'admin/structure/block');
      $new_row[] = implode(', ', $list_labels);
      // Now tack on the remaining built-in rows:
      $row = array_merge($new_row, $row);
    }

    $new_header[] = array_shift($render['#header']);
    $new_header[] = t('Display Mode(s)');
    $new_header[] = t('Unisender Lists');
    $render['#header'] = array_merge($new_header, $render['#header']);

    return $render;
  }

}
