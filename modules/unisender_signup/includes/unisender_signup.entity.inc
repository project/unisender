<?php

/**
 * @file
 * Unisender Signup entity class
 */

class UnisenderSignup extends Entity {
  public
    $uni_id,
    $name,
    $lists,
    $title,
    $settings;

  /**
   * Override parent constructor to set the entity type.
   */
  public function __construct(array $values = array()) {
    // The Features module calls the entity controller with all values cast to
    // an array. The fields and its settings are returned by the Unisender
    // API as objects. When constructing the entity from the Features import,
    // cast the fields back to objects for a consistent data model.
    if (!empty($values['settings']['fields'])) {
      foreach ($values['settings']['fields'] as $tag => $field) {
        if (is_array($field)) {
          if (is_array($field['options'])) {
            $field['options'] = (object) $field['options'];
          }
          $values['settings']['fields'][$tag] = (object) $field;
        }
      }
    }

    parent::__construct($values, 'unisender_signup');
  }

  /**
   * Return a label for a signup form.
   */
  public function label() {
    return $this->title;
  }

  /**
   * Overrides Entity\Entity::uri().
   */
  public function uri() {
    return array(
      'path' => 'admin/config/services/unisender/manage/' . $this->name,
      'options' => array(
        'entity_type' => $this->entityType,
        'entity' => $this,
      ),
    );
  }

}
