<?php

/**
 * @file
 * unisender_signup module admin settings.
 */

/**
 * Return a form for adding/editing a Unisender signup form.
 */
function unisender_signup_form($form, &$form_state, UnisenderSignup $signup) {
  // Store the existing list for updating on submit.
  $form_state['signup'] = $signup;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#description' => t('The title for this signup form.'),
    '#size' => 64,
    '#maxlength' => 256,
    '#default_value' => $signup->title,
    '#required' => TRUE,
  );

  // Machine-readable list name.
  $status = isset($signup->status) && $signup->uni_id && (($signup->status & ENTITY_IN_CODE) || ($signup->status & ENTITY_FIXED));
  $form['name'] = array(
    '#type' => 'machine_name',
    '#default_value' => $signup->name,
    '#maxlength' => 32,
    '#disabled' => $status,
    '#machine_name' => array(
      'exists' => 'unisender_signup_load_multiple_by_name',
      'source' => array('title'),
    ),
    '#description' => t('A unique machine-readable name for this list. It must only contain lowercase letters, numbers, and underscores.'),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($signup->settings['description']) ? $signup->settings['description'] : '',
    '#rows' => 2,
    '#maxlength' => 500,
    '#description' => t('This description will be shown on the signup form below the title. (500 characters or less)'),
  );
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['settings']['submit_button'] = array(
    '#type' => 'textfield',
    '#title' => t('Submit Button Label'),
    '#required' => 'TRUE',
    '#default_value' => isset($signup->settings['submit_button']) ? $signup->settings['submit_button'] : t('Submit'),
  );

  $form['settings']['confirmation_message'] = array(
    '#type' => 'textfield',
    '#title' => t('Confirmation Message'),
    '#description' => t('This message will appear after a successful submission of this form. Leave blank for no message, but make sure you configure a destination in that case unless you really want to confuse your site visitors.'),
    '#default_value' => isset($signup->settings['confirmation_message']) ? $signup->settings['confirmation_message'] : t('You have been successfully subscribed.'),
  );

  $form['settings']['destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Form destination page'),
    '#description' => t('Leave blank to stay on the form page.'),
    '#default_value' => isset($signup->settings['destination']) ? $signup->settings['destination'] : NULL,
  );

  $form['lists_config'] = array(
    '#type' => 'fieldset',
    '#title' => t('Unisender List Selection & Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $lists = unisender_get_lists();
  $form['lists_config']['lists'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Unisender Lists'),
    '#options' => array_column($lists, 'title', 'id'),
    '#default_value' => is_array($signup->lists) ? $signup->lists : array(),
    '#required' => TRUE,
    '#ajax' => array(
      'callback' => 'unisender_signup_fields_callback',
      'wrapper' => 'fields-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
      'progress' => array(
        'type' => 'throbber',
        'message' => t('Retrieving fields for this list.'),
      ),
    ),
  );

  $form['lists_config']['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Field Display'),
    '#description' => t('Select the fields to show on registration forms. Required fields are automatically displayed.'),
    '#id' => 'fields-wrapper',
    '#tree' => TRUE,
    '#weight' => 20,
  );
  $list_fields = unisender_get_fields();
  $form_state['field_options'] = array();
  foreach ($list_fields as $field) {
    $form_state['field_options'][$field['name']] = $field;
    if (!$field['is_visible']) {
      $form_state['field_options'][$field['name']]['public_name'] .= ' (private)';
    }
  }
  foreach ($form_state['field_options'] as $field) {
    $form['lists_config']['fields'][$field['name']] = array(
      '#type' => 'checkbox',
      '#title' => check_plain($field['public_name']),
      '#default_value' => isset($signup->settings['fields'][$field['name']]) ? !empty($signup->settings['fields'][$field['name']]) : TRUE,
      '#required' => isset($field['required']) && $field['required'],
      '#disabled' => isset($field['required']) && $field['required'],
    );
  }

  $form['subscription_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription Settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['subscription_settings']['doublein'] = array(
    '#type' => 'checkbox',
    '#title' => t('Require subscribers to Double Opt-in'),
    '#description' => t('New subscribers will be sent a link with an email they must follow to confirm their subscription.'),
    '#default_value' => isset($signup->settings['doublein']) ? $signup->settings['doublein'] : FALSE,
  );

  $form['subscription_settings']['include_tags'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include tags on subscription form.'),
    '#default_value' => isset($signup->settings['include_tags']) ? $signup->settings['include_tags'] : FALSE,
    '#description' => t('If set, subscribers will be able to select applicable tags on the signup form.'),
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#access' => isset($signup),
    '#submit' => array('unisender_signup_delete_submit'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/services/unisender/signup',
  );

  return $form;
}

/**
 * AJAX callback to return fields for a given type.
 *
 * Returns the form lists configuration fields.
 */
function unisender_signup_fields_callback($form, $form_state) {
  return $form['lists_config']['fields'];
}

/**
 * Submit handler for unisender_signup_form().
 */
function unisender_signup_form_submit($form, &$form_state) {
  if (isset($form_state['signup'])) {
    $signup = $form_state['signup'];
    $prior_settings = $signup->settings;
  }
  else {
    $signup = unisender_signup_create();
    $prior_settings = array();
  }
  $fields = $form_state['values']['fields'];
  foreach ($fields as $id => $val) {
    if ($val) {
      $fields[$id] = $form_state['field_options'][$id];
    }
  }
  $signup->title = $form_state['values']['title'];
  $signup->name = $form_state['values']['name'];
  $signup->lists = array_filter($form_state['values']['lists']);
  $signup->settings = $form_state['values']['settings'];
  $signup->settings['fields'] = $fields;
  $signup->settings['description'] = $form_state['values']['description'];
  $signup->settings['doublein'] = $form_state['values']['doublein'];
  $signup->settings['include_tags'] = $form_state['values']['include_tags'];
  if ($signup->save()) {
    // update i18n translation sources
    $language = language_default('language');
    $t_strings = array('title', 'name', 'description');
    foreach($t_strings as $key){
      unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:$key", $form_state['values'][$key], $language, TRUE);
    }
    foreach ($fields as $id => $val) {
      if ($val) {
        unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:field:$id", $val->name, $language, TRUE);
      }
    }
    unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:confirmation_message", $form_state['values']['settings']['confirmation_message'], $language, TRUE);
    unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:submit_button", $form_state['values']['settings']['submit_button'], $language, TRUE);

    drupal_set_message(t('Signup form @name has been saved.',
      array('@name' => $signup->name)));
    $form_state['redirect'] = 'admin/config/services/unisender/signup';
  }
  else {
    drupal_set_message(t('There has been an error saving your signup form.'), 'error');
  }
}

/**
 * Signup deletion form.
 */
function unisender_signup_signup_delete_form($form, &$form_state, $signup) {
  $form_state['signup'] = $signup;
  return confirm_form($form,
    t('Are you sure you want to delete the signup form %name?', array('%name' => $signup->label())),
    'admin/config/services/unisender/signup/' . $signup->identifier() . '/edit',
    t('This action cannot be undone.'),
    t('Delete Signup form'));
}

/**
 * Submit function for the delete button on the signup overview and edit forms.
 */
function unisender_signup_delete_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/config/services/unisender/signup/' . $form_state['signup']->identifier() . '/delete';
}

/**
 * Submit handler for unisender_signup_signup_delete_form().
 */
function unisender_signup_signup_delete_form_submit($form, &$form_state) {
  $signup = $form_state['signup'];

  $signup_id = $signup->identifier();

  // Deletes block.
  db_delete('block')
    ->condition('delta', $signup_id)
    ->condition('module', 'unisender_signup')
    ->execute();

  unisender_signup_delete_multiple(array($signup_id));
  drupal_set_message(t('%name has been deleted.', array('%name' => $signup->label())));
  $form_state['redirect'] = 'admin/config/services/unisender/signup';
}

/**
 * Create a new Unisender Signup object.
 *
 * @param array $values
 *   Associative array of values.
 *
 * @return UnisenderSignup
 *   New UnisenderSignup entity.
 */
function unisender_signup_create(array $values = array()) {
  return entity_get_controller('unisender_signup')->create($values);
}
