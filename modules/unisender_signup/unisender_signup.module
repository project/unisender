<?php

/**
 * @file
 * Unisender Signup module. Allows creation of signup forms integrated with
 * Unisender.
 */

/**
 * Implements hook_menu().
 */
function unisender_signup_menu() {
  $items = array();

  $items['admin/config/services/unisender/signup/%unisender_signup/edit'] = array(
    'title' => 'Edit a signup form',
    'description' => 'Edit a Signup form.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('unisender_signup_signup_form', 5),
    'load arguments' => array(5),
    'access arguments' => array('administer unisender'),
    'file' => 'includes/unisender_signup.admin.inc',
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/services/unisender/signup/manage/%unisender_signup/delete'] = array(
    'title' => 'Delete Signup form',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('unisender_signup_signup_delete_form', 6),
    'access arguments' => array('administer unisender'),
    'file' => 'includes/unisender_signup.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function unisender_signup_permission() {
  $return = array();
  $return['administer unisender signup entities'] = array(
    'title' => t('Administer Unisender Signup Entities.'),
  );

  return $return;
}

/**
 * Implements hook_block_info().
 */
function unisender_signup_block_info() {
  $blocks = array();

  $signups = unisender_signup_load_multiple();
  foreach ($signups as $signup) {
    $blocks[$signup->identifier()] = array(
      'info' => t('Unisender Subscription Form: @name', array('@name' => $signup->label())),
      'cache' => DRUPAL_CACHE_PER_USER,
    );
  }
  
  return $blocks;
}

/**
 * Implements hook_block_view().
 *
 * Provides a block for each signup configured to add a block.
 */
function unisender_signup_block_view($delta = '') {
  $signup = unisender_signup_load($delta);
  if (!$signup) {
    return FALSE;
  }

  $block = array(
    'subject' => check_plain($signup->title),
    'content' => drupal_get_form('unisender_signup_subscribe_block_' . $signup->name . '_form', $signup, 'unisender_signup_block'),
  );
  return $block;
}

/**
 * Wrapper around unisender_signup_load_multiple to load a single signup entity.
 */
function unisender_signup_load($signup_id) {
  $signups = unisender_signup_load_multiple(array($signup_id));
  return $signups ? reset($signups) : FALSE;
}

/**
 * Loads multiple signup forms.
 */
function unisender_signup_load_multiple($signup_ids = array(), $conditions = array(), $reset = FALSE) {
  if (empty($signup_ids)) {
    $signup_ids = FALSE;
  }

  return entity_load('unisender_signup', $signup_ids, $conditions, $reset);
}

/**
 * Deletes multiple signups by ID.
 *
 * @param array $signup_ids
 *   An array of signup IDs to delete.
 *
 * @return bool
 *   TRUE on success, FALSE otherwise.
 */
function unisender_signup_delete_multiple($signup_ids) {
  return entity_get_controller('unisender_signup')->delete($signup_ids);
}

/**
 * Gets an array of all Signups, keyed by the list name.
 *
 * @param string $name
 *   If set, the signup with the given name is returned.
 *
 * @return UnisenderSignup[]
 *   Depending whether $name isset, an array of signups or a single one.
 */
function unisender_signup_load_multiple_by_name($name = NULL) {
  $signups = entity_load_multiple_by_name('unisender_signup', isset($name) ? array($name) : FALSE);
  return isset($name) ? reset($signups) : $signups;
}

/**
 * Implements hook_entity_info().
 */
function unisender_signup_entity_info() {
  $entities = array(
    'unisender_signup' => array(
      'label' => t('Unisender Signup'),
      'plural label' => t('Unisender Signups'),
      'entity class' => 'UnisenderSignup',
      'controller class' => 'EntityAPIControllerExportable',
      'base table' => 'unisender_signup',
      'uri callback' => 'entity_class_uri',
      'fieldable' => FALSE,
      'exportable' => TRUE,
      'label callback' => 'unisender_signup_entity_info_label',
      'module' => 'unisender_signup',
      'entity keys' => array(
        'id' => 'uni_id',
        'name' => 'name',
      ),
      'bundles' => array(
        'unisender_signup' => array(
          'label' => t('Unisender Signup'),
        ),
      ),
      'view modes' => array(
        'full' => array(
          'label' => t('Full'),
          'custom settings' => FALSE,
        ),
      ),
      // Enable the entity API's admin UI.
      'admin ui' => array(
        'path' => 'admin/config/services/unisender/signup',
        'file' => 'includes/unisender_signup.admin.inc',
        'controller class' => 'UnisenderSignupUIController',
      ),
      'access callback' => 'unisender_signup_entity_access',
    ),
  );

  return $entities;
}

/**
 * Entity label callback.
 */
function unisender_signup_entity_info_label($entity, $entity_type) {
  return empty($entity) ? 'New Uninder Signup form' : $entity->title;
}

/**
 * Access callback for unisender_signup_entity.
 */
function unisender_signup_entity_access() {
  return unisender_apikey_ready_access('administer unisender signup entities');
}

/**
 * Menu callback for Signup pages.
 */
function unisender_signup_page($signup_id) {
  $signup = unisender_signup_load($signup_id);
  return drupal_get_form('unisender_signup_subscribe_page_' . $signup_id . '_form', $signup, 'unisender_signup_page');
}

/**
 * Implements hook_forms().
 *
 * This allows each subscription form rendering to have a unique form
 * ID. If this weren't the case, multiple forms getting rendered on a single
 * page display would have submit button conflicts.
 */
function unisender_signup_forms($form_id, $args) {
  $forms = array();
  if (strpos($form_id, 'unisender_') === 0) {
    $forms['unisender_signup_subscribe_block_' . $args[0]->name . '_form'] = array(
      'callback' => 'unisender_signup_subscribe_form',
    );
  }
  return $forms;
}

/**
 * Returns a subscription form for unisender lists.
 *
 * If there are multiple lists, this generates a single form for all of them.
 */
function unisender_signup_subscribe_form($form, &$form_state, $signup, $type) {
  $form['#attributes'] = array('class' => array('unisender-signup-subscribe-form'));
  
  // Allowing node tokens in block description.
  if (!empty($signup->settings['description'])) {
    // Get node (current article) id.
    $node_id = arg(1);
    if (!empty($node_id) && is_numeric($node_id)) {
      $signup->settings['description'] = token_replace($signup->settings['description'], array('node' => node_load($node_id)));
    }
  }

  $form['description'] = array(
    '#markup' => unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:description", filter_xss($signup->settings['description'])),

    '#prefix' => '<div class="unisender-signup-subscribe-form-description">',
    '#suffix' => '</div>'
  );
  $form['unisender_lists'] = array('#tree' => TRUE);
  $all_lists = unisender_get_lists();
  $lists = array();
  foreach ($all_lists as $id => $name) {
    if (in_array($id, $signup->lists)) {
      $lists[$id] = $name;
    }
  }
  $lists_count = !empty($lists) ? count($lists) : 0;

  if (empty($lists)) {
    drupal_set_message('The subscription service is currently unavailable. Please try again later.', 'warning');
  }

  if ($lists_count > 1) {
    foreach ($lists as $list) {
      // Wrap in a div:
      $form['unisender_lists'][$list['id']] = array(
        '#type' => 'checkbox',
        '#title' => $list['title'],
        '#return_value' => $list['id'],
        '#default_value' => 0,
        '#prefix' => '<div id="unisender-newsletter-' . $list['id'] . '" class="unisender-newsletter-wrapper">',
        '#suffix' => '</div>',
      );
    }
  }

  if ($signup->settings['include_tags']) {
    $form['tags'] = unisender_tags_form_elements();
    $form['tags']['#weight'] = 9;
  }

  $form['fields'] = array(
    '#prefix' => '<div id="unisender-newsletter-fields" class="unisender-newsletter-fields">',
    '#suffix' => '</div>',
    '#tree' => TRUE,
  );
  foreach ($signup->settings['fields'] as $name => $field) {
    if (!empty($field)) {
      $field['public_name'] = unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:field:$name", $field['public_name']);
      $form['fields'][$name] = unisender_insert_drupal_form_tag($field);
      if (empty($lists)) {
        $form['fields'][$name]['#disabled'] = TRUE;
      }
    }
  }

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#weight' => 10,
    '#value' => unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:submit_button", $signup->settings['submit_button']),
    '#disabled' => empty($lists),
  );

  return $form;
}

/**
 * Validation handler for unisender_signup_subscribe_form.
 */
function unisender_signup_subscribe_form_validate($form, &$form_state) {
  $signup = reset($form_state['build_info']['args']);
  if (count($signup->lists) > 1) {
    foreach ($form_state['values']['unisender_lists'] as $list) {
      if ($list) {
        return;
      }
    }
    form_set_error('unisender_lists', t("Please select at least one list to subscribe to."));
  }
}

/**
 * Submit handler to add users to lists on subscription form submission.
 */
function unisender_signup_subscribe_form_submit($form, &$form_state) {
  $signup = reset($form_state['build_info']['args']);
  $lists_ids = count($signup->lists) > 1
          ? array_filter($form_state['values']['unisender_lists'])
          : reset($signup->lists);
  // Filter out blank fields so we don't erase values on the Unisender side.
  $fields = array_filter($form_state['values']['fields']);
  $tags = $form_state['values']['tags'] ?? [];
  $email = $fields['email'];

  $result = unisender_subscribe($lists_ids, $email, $fields, $tags, $signup->settings['doublein']);
  // Let other modules act on the results in hook_form_alter.
  $form_state['unisender_results'] = $result;
  if (empty($result)) {
    drupal_set_message(t('There was a problem with your newsletter signup'), 'warning');
  }
  elseif (isset($signup->settings['confirmation_message']) && strlen($signup->settings['confirmation_message'])) {
    $message = unisender_signup_tt("field:unisender_signup:form:$signup->uni_id:confirmation_message", check_plain($signup->settings['confirmation_message']));
    drupal_set_message($message, 'status');
  }
  if (!empty($signup->settings['destination'])) {
    $form_state['redirect'] = $signup->settings['destination'];
  }
}

/**
 * Wrapper function for i18n_string() if i18nstrings enabled.
 */
function unisender_signup_tt($name, $string, $langcode = NULL, $update = FALSE) {
  if (function_exists('i18n_string')) {
    $options = array(
      'langcode' => $langcode,
      'update' => $update,
    );
    return i18n_string($name, $string, $options);
  }

  return $string;
}
