<?php

/**
 * @file
 * Install, update and uninstall functions for the unisender_signup module.
 */

/**
 * Implements hook_schema().
 */
function unisender_signup_schema() {
  $schema['unisender_signup'] = array(
    'fields' => array(
      'uni_id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique unisender_signup entity ID.',
      ),
      'name' => array(
        'description' => 'The machine-readable name of this unisender_signup.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'lists' => array(
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized array of list IDs with list-specific configuration.',
      ),
      'title' => array(
        'type' => 'varchar',
        'length' => 256,
        'not null' => TRUE,
        'default' => '',
      ),
      'settings' => array(
        'type' => 'blob',
        'not null' => FALSE,
        'size' => 'big',
        'serialize' => TRUE,
        'description' => 'A serialized object that stores the settings for the specific list.',
      ),
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        // Set the default to ENTITY_CUSTOM without using the constant as it is
        // not safe to use it at this point.
        'default' => 0x01,
        'size' => 'tiny',
        'description' => 'The exportable status of the entity.',
      ),
      'module' => array(
        'description' => 'The name of the providing module if the entity has been defined in code.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => FALSE,
      ),
    ),
    'primary key' => array('uni_id'),
    'unique key' => array('name'),
  );

  return $schema;
}

/**
 * Enlarging Unisender Signup title field.
 */
function unisender_signup_update_7000() {
  if (db_field_exists('unisender_signup', 'title')) {
    $db_field_info = array(
      'type' => 'varchar',
      'length' => 256,
      'not null' => TRUE,
      'default' => '',
    );
    db_change_field('unisender_signup', 'title', 'title', $db_field_info);
  }
}