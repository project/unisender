<?php

/**
 * @file
 * unisender_lists module admin settings.
 */

/**
 * Administrative display showing existing lists and allowing edits/adds.
 */
function unisender_lists_overview_page() {
  $lists = unisender_get_lists();
  
  $rows = array();
  foreach ($lists as $list_id => $list_info) {
    $rows[] = array(
      $list_info['title'],
      $list_info['members_count'],
    );
  }

  $refresh_link = l(t('Refresh lists from Unisender'),
    'admin/config/services/unisender/list_cache_clear',
    array('query' => array('destination' => 'admin/config/services/unisender/lists')));

  $webhooks = unisender_unsubscribe_webhook_get();
  if ($webhooks) {
    // This is a webhook for this Unisender Module.
    $webhook_status = t('ENABLED') . ' (' . l(t('disable'), 'admin/config/services/unisender/lists/' . $list_id . '/webhook/disable') . ')';
  }
  else {
    $webhook_status = t('disabled') . ' (' . l(t('enable'), 'admin/config/services/unisender/lists/' . $list_id . '/webhook/enable') . ')';
  }

  $build['table'] = array(
    '#theme' => 'table',
    '#header' => array(
      t('Name'),
      t('Members'),
    ),
    '#rows' => $rows,
    '#caption' => $refresh_link,
    '#suffix' => "Webhook " . $webhook_status,
  );
  

  if (empty($lists)) {
    drupal_set_message(t('You don\'t have any lists configured in your Unisender account, (or you haven\'t configured your API key correctly on the Global Settings tab). Head over to Unisender and create some lists, then come back here and click "Refresh lists from Unisender"'), 'warning');
  }

  return drupal_render($build);
}

/**
 * Administrative display showing existing lists and allowing edits/adds.
 */
function unisender_lists_fields_overview_page() {
  $fields = field_info_fields();
  $rows = array();
  foreach ($fields as $field) {
    if ($field['type'] == 'unisender_lists_subscription') {
      foreach ($field['bundles'] as $entity_type => $bundles) {
        foreach ($bundles as $bundle) {
          $link = 'admin/config/services/unisender/lists/update_fields/' . $entity_type . '/' . $bundle . '/' . $field['field_name'];
          $rows[] = array(
            $entity_type,
            $bundle,
            $field['field_name'],
            l(t('Update Unisender Fields Values'), $link),
          );
        }
      }
    }
  }
  $table = array(
    'header' => array(
      t('Entity Type'),
      t('Bundle'),
      t('Field'),
      t('Batch Update'),
    ),
    'rows' => $rows,
  );

  $table['caption'] = t("This displays a list of all Unisender Subscription Fields configured on your system, with a row for each unique Instance of that field.
    To edit each field's settings, go to the Entity Bundle's configuration screen and use the Field UI.
    When entities with Unisender Subscription Fields are updated, the Merge Variables configured through Field UI are automatically updated if necessary.
    However, if you have existing subscribers on Unisender and matching Entities on Drupal when you configure your Merge Variables, the existing values are not synced automatically,
    as this could be a slow process. You can manually force updates of all existing Merge Values to existing Unisender subscribers for each field configuration using the 'Batch Update'
    option on this table. The Unisender Subscription Field is provided by the Unisender Lists (unisender_lists) module.<br/><br/>
    The following Unisender Subscription Field instances were found:");

  return theme('table', $table);
}

/**
 * Webhooks Toggle form.
 */
function unisender_lists_webhook_form($form, &$form_state, $list_id, $action) {
  $form_state['action'] = $action;

  switch ($action) {
    case "enable":
      break;

    case "disable":
      break;

    default:
      return array();
  }
  $label = t($action) . " webhooks";
  return confirm_form($form,
    t('Are you sure you want to &action unsubscribe webhook?',
      array(
        '&action' => t($action),
      )),
    'admin/config/services/unisender/lists',
    t('You can change this setting back from the unisender administrative UI.'),
    $label
  );
}

/**
 * Submit handler for unisender_lists_webhook_form().
 */
function unisender_lists_webhook_form_submit($form, &$form_state) {
  switch ($form_state['action']) {
    case 'enable':
      $result = unisender_unsubscribe_webhook_add(unisender_webhook_url());
      break;

    case 'disable':
      $result = unisender_unsubscribe_webhook_delete(unisender_webhook_url());
      break;

    default:
      $result = FALSE;
      break;

  }
  if ($result) {
    drupal_set_message(t('Webhook have been %action.',
      array(
        '%action' => $form_state['action'] . 'd',
      )
    ));
  }
  else {
    drupal_set_message(t('Unable to perform webhook action "%action".',
      array(
        '%action' => $form_state['action'],
      )
    ), 'warning');

  }
  $form_state['redirect'] = 'admin/config/services/unisender/lists';
}


/**
 * Update Fields Form.
 */
function unisender_lists_update_fields_form($form, &$form_state, $entity_type, $bundle_name, $field_name) {
  $field = field_info_field($field_name);
  $form_state['entity_type'] = $entity_type;
  $form_state['bundle_name'] = $bundle_name;
  $form_state['field'] = $field;

  return confirm_form($form,
    t('Update Fields for all @entity_type -- @bundle entities?',
      array(
        '@entity_type' => $entity_type,
        '@bundle' => $bundle_name,
      )),
    'admin/config/services/unisender/lists',
    t('This can overwrite values configured directly on your Unisender Account.'),
    "Update Fields"
  );
}

/**
 * Submit handler for unisender_lists_update_fields_form().
 */
function unisender_lists_update_fields_form_submit($form, &$form_state) {
  unisender_lists_update_member_fields_values($form_state['entity_type'], $form_state['bundle_name'], $form_state['field']);
  $form_state['redirect'] = 'admin/config/services/unisender/fields';
}
