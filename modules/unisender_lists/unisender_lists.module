<?php

/**
 * @file
 * Unisender lists module.
 */

module_load_include('inc', 'unisender_lists', 'includes/unisender_lists.field');

/**
 * Implements hook_menu().
 */
function unisender_lists_menu() {
  $items = array();

  $items['admin/config/services/unisender/lists'] = array(
    'title' => t('Lists'),
    'description' => t('Display Unisender Lists and configure Webhooks.'),
    'page callback' => 'unisender_lists_overview_page',
    'access callback' => 'unisender_apikey_ready_access',
    'access arguments' => array('administer unisender'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/unisender_lists.admin.inc',
    'weight' => 10,
  );
  $items['admin/config/services/unisender/list_cache_clear'] = array(
    'page callback' => 'unisender_lists_cache_clear',
    'access callback' => 'unisender_apikey_ready_access',
    'access arguments' => array('administer unisender'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/services/unisender/fields'] = array(
    'title' => t('Fields'),
    'description' => t('Display Unisender Subscription Fields and trigger Fields updates.'),
    'page callback' => 'unisender_lists_fields_overview_page',
    'access callback' => 'unisender_apikey_ready_access',
    'access arguments' => array('administer unisender'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'includes/unisender_lists.admin.inc',
    'weight' => 10,
  );
  $items['admin/config/services/unisender/lists/%/webhook/%'] = array(
    'title' => t('Enable/disable Webhooks'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('unisender_lists_webhook_form', 5, 7),
    'access callback' => 'unisender_apikey_ready_access',
    'access arguments' => array('administer unisender'),
    'file' => 'includes/unisender_lists.admin.inc',
  );
  $items['admin/config/services/unisender/lists/update_fields/%/%/%'] = array(
    'title' => t('Mass Update Fields'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('unisender_lists_update_fields_form', 6, 7, 8),
    'access callback' => 'unisender_apikey_ready_access',
    'access arguments' => array('administer unisender'),
    'file' => 'includes/unisender_lists.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_entity_delete().
 */
function unisender_lists_entity_delete($entity, $type) {
  // Check for unisender subscription fields and unsubscribe accordingly.
  $fields = field_info_fields();
  foreach ($fields as $field) {
    if ($field['type'] === 'unisender_lists_subscription' && isset($field['bundles'][$type])) {
      $wrapper = entity_metadata_wrapper($type, $entity);
      if (in_array($wrapper->getBundle(), $field['bundles'][$type], TRUE)) {
        $instance = field_info_instance($type, $field['field_name'], $wrapper->getBundle());
        if ($instance['settings']['unsubscribe_on_delete']) {
          unisender_lists_process_subscribe_form_choices(array('subscribe' => FALSE), $instance, $field, $entity);
        }
      }
    }
  }
}

/**
 * Helper function to check if a valid email is configured for an entity field.
 *
 * Returns an email address or FALSE.
 */
function unisender_lists_load_email($instance, $entity, $log_errors = TRUE) {
  // Load original email if it exists.
  if (isset($entity->original)) {
    $email = unisender_lists_load_email($instance, $entity->original, $log_errors);
    if ($email) {
      return $email;
    }
  }

  if (!isset($instance['settings']['fields']['email'])) {
    if ($log_errors) {
      watchdog('unisender_lists', 'Unisender Lists field "@field" on @entity -> @bundle has no "email" field configured, subscription actions cannot take place.', array(
        '@field' => $instance['field_name'],
        '@entity' => $instance['entity_type'],
        '@bundle' => $instance['bundle'],
      ), WATCHDOG_NOTICE);
    }
    return FALSE;
  }

  // This function can be called when creating a form for a new entity, in which
  // case there will be no existing entity to inspect in order to figure out
  // what the email address is. For example, the user registration form. So we
  // need to skip the rest of this code if there is no ID for the entity.
  list($id, $vid, $bundle) = entity_extract_ids($instance['entity_type'], $entity);
  if (!isset($id) || $id === 0) {
    return FALSE;
  }

  // Expand the token used in the email field.
  $tokens = array();
  foreach ($instance['settings']['fields'] as $key => $token) {
    $tokens[$key] = token_replace($token, array($instance['entity_type'] => $entity), array('clear' => TRUE));
  }
  $email = $tokens['email'];

  if (valid_email_address($email)) {
    return $email;
  }

  return FALSE;
}

/**
 * Implements hook_forms().
 * 
 * This allows each field's subscription-form rendering to have a unique form
 * ID. If this weren't the case, multiple forms getting rendered on a single
 * entity display would have submit button conflicts.
 */
function unisender_lists_forms($form_id, $args) {
  $forms = array();
  if (strpos($form_id, 'unisender_lists_') === 0 && isset($args[3]['type']) && $args[3]['type'] == "unisender_lists_subscription") {
    $forms['unisender_lists_' . $args[0]['field_name'] . '_form'] = array(
      'callback' => 'unisender_lists_subscribe_form',
    );
  }
  return $forms;
}

/**
 * Return a form element for a single newsletter.
 */
function unisender_lists_subscribe_form($form, &$form_state, $instance, $settings, $entity, $field) {
  $list = unisender_get_list($field['settings']['list_id']);
  $email = unisender_lists_load_email($instance, $entity);
  if (!$email) {
    return array();
  }
  // Determine if a user is subscribed to the list.
  $is_subscribed = unisender_is_subscribed($list->id, $email);
  $wrapper_key = 'unisender_' . $instance['field_name'];
  $form_state['settings'] = array(
    'wrapper_key' => $wrapper_key,
    'instance' => $instance,
    'field' => $field,
    'entity' => $entity,
  );
  $form[$wrapper_key] = array(
    '#type' => 'container',
    '#tree' => TRUE,
    '#description' => $instance['description'],
    '#attributes' => array(
      'class' => array(
        'unisender-newsletter-wrapper',
        'unisender-newsletter-' . $instance['field_name'],
      ),
    ),
  );
  // Add the title and description to lists for anonymous users or if requested:
  $form[$wrapper_key]['subscribe'] = array(
    '#type' => 'checkbox',
    '#title' => t('Subscribe'),
    '#disabled' => $instance['required'],
    '#required' => $instance['required'],
    '#default_value' => $instance['required'] || $is_subscribed,
  );
  // Present tags:
  if ($instance['settings']['show_tags'] && $settings['show_tags']) {
    // Perform test in case error comes back from API when getting groups:
    $form[$wrapper_key]['tags'] = array(
      '#type' => 'fieldset',
      '#title' => $settings['tags_label'] ?? t('Tags'),
      '#weight' => 100,
      '#states' => array(
        'invisible' => array(
          ':input[name="' . $wrapper_key . '[subscribe]"]' => array('checked' => FALSE),
        ),
      ),
    );
    $form[$wrapper_key]['tags'] += unisender_tags_form_elements($list, $instance['default_value'][0]['tags'], $email);
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Submit handler to add emails to lists when editing/creating an entity.
 */
function unisender_lists_subscribe_form_submit($form, &$form_state) {
  unisender_lists_process_subscribe_form_choices(
    $form_state['values'][$form_state['settings']['wrapper_key']],
    $form_state['settings']['instance'],
    $form_state['settings']['field'],
    $form_state['settings']['entity']);
}

/**
 * Processor for various list form submissions.
 *
 * Subscription blocks, user settings, and new user creation.
 * 
 * @array $choices
 *   An array representing the form values selected.
 * @array $instance
 *   A unisender_lists_subscription field instance configuration.
 * @array $field
 *   A unisender_lists_subscription field definition.
 * @entity $entity
 *   An Entity that has the $instance field.
 */
function unisender_lists_process_subscribe_form_choices($choices, $instance, $field, $entity) {
  $email = unisender_lists_load_email($instance, $entity);
  if (!$email) {
    // We can't do much subscribing without an email address.
    return;
  }
  $function = FALSE;
  $list_id = $field['settings']['list_id'];
  $subscribed = unisender_is_subscribed($list_id, $email);
  if ($choices['subscribe'] != $subscribed) {
    // Subscription selection has changed.
    if ($choices['subscribe']) {
      $function = 'add';
    }
    elseif (isset($field['settings']['unsubscribe_action'])) {
      $function = $field['settings']['unsubscribe_action'];
    }
    else {
      $function = 'remove';
    }
  }
  elseif ($choices['subscribe']) {
    $function = 'update';
  }
  if ($function) {
    if ($function === 'remove') {
      $fields = array();
    }
    else {
      $fields = _unisender_lists_fields_populate($instance['settings']['fields'], $entity, $instance['entity_type']);
    }

    $tags = $choices['tags'] ?? [];

    switch ($function) {
      case 'add':
        $ret = unisender_subscribe(array($list_id), $email, $fields, $tags, $field['settings']['double_opt_in']);
        break;

      case 'unsubscribe':
        $ret = unisender_unsubscribe($list_id, $email, FALSE);
        break;

      case 'remove':
        $ret = unisender_unsubscribe($list_id, $email, TRUE);
        break;

      case 'update':
        if (_unisender_lists_subscription_has_changed($instance, $field, $entity, $email, $choices)) {
          $ret = unisender_update_member($list_id, $email, $fields, $tags);
        }
        else {
          $ret = TRUE;
        }
        break;

    }
    if (empty($ret)) {
      drupal_set_message(t('There was a problem with your newsletter signup.'), 'warning');
    }
  }
}

/**
 * Helper function to avoid sending superfluous updates to Unisender.
 *
 * This is necessary due to the nature of the field implementation of
 * subscriptions. If we don't do this, we send an update to Unisender every time
 * an entity is updated.
 * 
 * returns bool
 */
function _unisender_lists_subscription_has_changed($instance, $field, $entity, $email, $choices) {
  if (isset($entity->original)) {
    $new_entity_wrapper = entity_metadata_wrapper($instance['entity_type'], $entity);
    $old_entity_wrapper = entity_metadata_wrapper($instance['entity_type'], $entity->original);
    // First compare InteresTags settings:
    if ($instance['settings']['show_tags']) {
      $old_settings = $old_entity_wrapper->{$field['field_name']}->value();
      $new_settings = $new_entity_wrapper->{$field['field_name']}->value();
      foreach ($new_settings['tags'] as $id => $name) {
        if (!isset($old_settings['tags'][$id])) {
          return TRUE;
        }
      }
    }

    // Expand tokens for both the new entity and the old one, then compare the
    // values to see if anything has changed.
    $fields_new = unisender_fields_populate($instance['settings']['fields'], $entity, $instance['entity_type']);
    $fields_old = unisender_fields_populate($instance['settings']['fields'], $entity->original, $instance['entity_type']);
    if ($fields_new != $fields_old) {
      return TRUE;
    }
  }
  // We don't have an old entity to compare values so we have to retrieve our
  // old settings from Unisender. This means the only possible change is in our
  // tags settings, so we only analyze those:
  else {
    $member_info = unisender_get_memberinfo($field['settings']['list_id'], $email);
    // @TODO: implment this
//    foreach ($choices['interest_groups'] as $interest_group) {
//      foreach ($interest_group as $interest_id => $interest_status) {
//        // Check for new interest selected.
//        if (($interest_status !== 0) && empty($member_info->interests->{$interest_id})) {
//          return TRUE;
//        }
//        // Check for existing interest unselected.
//        if (($interest_status === 0) && !empty($member_info->interests->{$interest_id})) {
//          return TRUE;
//        }
//      }
//    }
  }
  // No changes detected:
  return FALSE;
}

/**
 * Helper function to complete a Unisender-API-ready fields array.
 */
function _unisender_lists_fields_populate($fields, $entity, $entity_type) {
  $uni_fields = unisender_fields_populate($fields, $entity, $entity_type);
  drupal_alter('unisender_lists_fields', $uni_fields, $entity, $entity_type);
  return $uni_fields;
}

/**
 * Triggers an update of all field values for appropriate entities.
 */
function unisender_lists_update_member_fields_values($entity_type, $bundle_name, $field) {
  $instance = field_info_instance($entity_type, $field['field_name'], $bundle_name);
  $uni_fields = $instance['settings']['fields'];
  // Assemble a list of current subscription statuses so we don't alter them.
  // Because of cacheing we don't want to use the standard checks. Expiring the
  // cache would kill the point of doing this as a batch API operation.
  $batch = array(
    'operations' => array(
      array('unisender_lists_get_subscribers', array($field)),
      array(
        'unisender_lists_populate_member_batch',
        array(
          $entity_type,
          $bundle_name,
          $field,
          $uni_fields,
        ),
      ),
      array('unisender_lists_execute_fields_batch_update', array($field['settings']['list_id'])),
    ),
    'finished' => 'unisender_lists_populate_member_batch_complete',
    'title' => t('Processing Fields Updates'),
    'init_message' => t('Starting Unisender Fields Update.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message' => t('Unisender Fields Update Failed.'),
  );
  batch_set($batch);
}

/**
 * Batch processor for pulling in subscriber information for a list.
 */
function unisender_lists_get_subscribers($field, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['results']['subscribers'] = array();
    $context['sandbox']['progress'] = 0;
  }
  $batch_size = variable_get('unisender_batch_limit', 100);
  $options = array(
    'offset' => $context['sandbox']['progress'] / $batch_size,
    'count' => $batch_size,
  );
  $matches = unisender_get_members($field['settings']['list_id'], 'subscribed', $options);
  if (!empty($matches) && ($matches->total_items > 0)) {
    if (!isset($context['sandbox']['max'])) {
      $context['sandbox']['max'] = $matches->total_items;
    }
    foreach ($matches->members as $result) {
      $context['results']['subscribers'][strtolower($result->email_address)] = $result;
    }
    $context['sandbox']['progress'] += count($matches->members);
    $context['message'] = t('Check subscription status for contact %count of %total.', array('%count' => $context['sandbox']['progress'], '%total' => $context['sandbox']['max']));
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch processor for member field updates to built the fields arrays.
 */
function unisender_lists_populate_member_batch($entity_type, $bundle_name, $field, $fields, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    // Load up all our eligible entities.
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $entity_type);
    if ($entity_type !== 'user') {
      $query->entityCondition('bundle', $bundle_name);
    }
    $query_results = $query->execute();
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = isset($query_results[$entity_type]) ? count($query_results[$entity_type]) : 0;
    if ($context['sandbox']['max']) {
      $context['sandbox']['entity_ids'] = array_keys($query_results[$entity_type]);
      $context['results']['update_queue'] = array();
    }
  }
  if ($context['sandbox']['progress'] < $context['sandbox']['max']) {
    $batch_size = variable_get('unisender_batch_limit', 100);
    $item_ids = array_slice($context['sandbox']['entity_ids'], $context['sandbox']['progress'], $batch_size);
    $entities = entity_load($entity_type, $item_ids);
    foreach ($entities as $entity) {
      $uni_fields = _unisender_lists_fields_populate($fields, $entity, $entity_type);
      if ($uni_fields['email'] && isset($context['results']['subscribers'][strtolower($uni_fields['email'])])) {
        $context['results']['update_queue'][] = array(
          'email' => $uni_fields['email'],
          'fields' => $uni_fields,
        );
      }
    }
    $context['sandbox']['progress'] += count($item_ids);
    $context['message'] = t('Checking for changes on items %count - %next.',
      array(
        '%count' => $context['sandbox']['progress'],
        '%next' => $context['sandbox']['progress'] + $batch_size,
      )
    );
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch processor for member fields updates to submit batches to Unisender.
 */
function unisender_lists_execute_fields_batch_update($list_id, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['total'] = count($context['results']['update_queue']);
    $context['results']['updates'] = 0;
    $context['results']['errors'] = 0;
  }
  if ($context['sandbox']['progress'] < $context['sandbox']['total']) {
    $batch_size = variable_get('unisender_batch_limit', 100);
    $batch = array_slice($context['results']['update_queue'], $context['sandbox']['progress'], $batch_size);
    $result = unisender_batch_update_members($list_id, $batch, FALSE, TRUE, FALSE);
    if ($result) {
      $mc = unisender_get_api();
      $batch_result = $mc->getBatchOperation($result->id);

      $context['results']['updates'] += count($context['results']['update_queue']);
      $context['results']['errors'] += $batch_result->errored_operations;
    }
    $context['sandbox']['progress'] += count($batch);
    $context['message'] = t('Updating Unisender fields for items %count - %next.',
      array(
        '%count' => $context['sandbox']['progress'],
        '%next' => $context['sandbox']['progress'] + $batch_size,
      )
    );
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['total'];
  }
}

/**
 * Batch completion processor for member mergevar updates.
 */
function unisender_lists_populate_member_batch_complete($success, $results, $operations) {
  if ($success) {
    if ($results['errors']) {
      drupal_set_message(t('Update errors occurred: merge variables updated on %count records, errors occurred on %errors records.',
        array(
          '%count' => $results['updates'],
          '%errors' => $results['errors'],
        )
      ), 'warning');
    }
    else {
      drupal_set_message(t('Merge variables updated on %count records.',
        array(
          '%count' => $results['updates'],
        )
      ), 'status');
    }
  }
  else {
    drupal_set_message(t('Merge variable update failed.'), 'error');
  }
}

/**
 * Get an array with all possible Drupal properties for a given entity type.
 *
 * @param string $entity_type
 *   Name of entity whose properties to list.
 * @param string $entity_bundle
 *   Optional bundle to limit available properties.
 * @param bool $required
 *   Set to TRUE if properties are required.
 * @param string $prefix
 *   Optional prefix for option IDs in the options list.
 * @param string $tree
 *   Optional name of the parent element if this options list is part of a tree.
 *
 * @return array
 *   List of properties that can be used as an #options list.
 */
function unisender_lists_fieldmap_options($entity_type, $entity_bundle = NULL, $required = FALSE, $prefix = NULL, $tree = NULL) {
  $options = array();
  if (!$prefix) {
    $options[''] = t('-- Select --');
  }

  $properties = entity_get_all_property_info($entity_type);
  if (isset($entity_bundle)) {
    $info = entity_get_property_info($entity_type);
    $properties = $info['properties'];
    if (isset($info['bundles'][$entity_bundle])) {
      $properties += $info['bundles'][$entity_bundle]['properties'];
    }
  }

  foreach ($properties as $key => $property) {
    // The entity_token module uses - instead of _ for all property and field
    // names so we need to ensure that we match that pattern.
    $key = str_replace('_', '-', $key);
    // Include the entity type at the beginning of token names since tokens use
    // the format [entity_type:entity_property].
    $keypath = $prefix ? $prefix . ':' . $key : $entity_type . ':' . $key;
    $type = isset($property['type']) ? entity_property_extract_innermost_type($property['type']) : 'text';
    $is_entity = ($type === 'entity') || (bool) entity_get_info($type);
    $required_property = $property['required'] ?? FALSE;
    $computed_property = $property['computed'] ?? FALSE;
    if ($is_entity) {
      // We offer fields on related entities (useful for field collections).
      // But we only offer 1 level of depth to avoid loops.
      if (!$prefix) {
        $bundle = $property['bundle'] ?? NULL;
        $options[$property['label']] = unisender_lists_fieldmap_options($type, $bundle, $required, $keypath, $property['label']);
      }
    }
    elseif (!$required || $required_property || $computed_property) {
      if (isset($property['field']) && $property['field'] && !empty($property['property info'])) {
        foreach ($property['property info'] as $sub_key => $sub_prop) {
          $label = isset($tree) ? $tree . ' - ' . $property['label'] : $property['label'];
          // Convert paths to full token syntax for parsing later.
          $token = '[' . $keypath . ']';
          $options[$label][$token] = $sub_prop['label'];
        }
      }
      else {
        // Convert paths to full token syntax for parsing later.
        $token = '[' . $keypath . ']';
        $options[$token] = $property['label'];
      }
    }
  }
  return $options;
}

/**
 * Implements hook_action_info().
 */
function unisender_lists_action_info() {
  $actions = array(
    'unisender_lists_add_to_list_action' => array(
      'type' => 'entity',
      'label' => t('Add to Unisender List'),
      'configurable' => FALSE,
      'triggers' => array('any'),
      'vbo_configurable' => TRUE,
    ),
  );
  return $actions;
}

/**
 * Action function for the Add To List action.
 * 
 * Does the actual subscription work. Builds a queue of list adds and then
 * intermittently builds a batched API action from the queue and sends to
 * Unisender.
 */
function unisender_lists_add_to_list_action($entity, $context = array()) {
  list($id, $vid, $bundle) = entity_extract_ids($context['entity_type'], $entity);
  $list_id = $context['list_id'];
  $field_instance = field_info_instance($context['entity_type'], $context['unisender_field'], $bundle);
  $email = unisender_lists_load_email($field_instance, $entity);
  if ($email) {
    unisender_subscribe($list_id, $email);
  }
}

/**
 * An action configuration form for performing Add To List actions.
 */
function unisender_lists_add_to_list_action_form($context, &$form_state) {
  $form = array();
  $unisender_field_options = array();
  $form_state['list_ids'] = array();
  foreach ($context['settings']['unisender_field'] as $field) {
    if (($info = field_info_field($field))) {
      $form_state['list_ids'][$field] = $info['settings']['list_id'];
      $bundle = reset($info['bundles'][$context['entity_type']]);
      $instance_info = field_info_instance($context['entity_type'], $field, $bundle);
      $unisender_field_options[$field] = t("@label (@fieldname)",
        array(
          '@label' => $instance_info['label'],
          '@fieldname' => $field,
        ));
    }
  }
  $form['unisender_field'] = array(
    '#type' => 'select',
    '#title' => t('Select a Unisender List'),
    '#options' => $unisender_field_options,
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Form handler for unisender_lists_add_to_list_action_form().
 */
function unisender_lists_add_to_list_action_submit($form, &$form_state) {
  $list_id = $form_state['list_ids'][$form_state['values']['unisender_field']];
  $values = array(
    'unisender_field' => $form_state['values']['unisender_field'],
    'list_id' => $list_id,
  );
  return $values;
}

/**
 * Implements hook_views_bulk_operations_form().
 *
 * Configuration option for adding VBO action
 */
function unisender_lists_add_to_list_action_views_bulk_operations_form($options, $entity_type, $dom_id) {
  $field_options = array();
  $fields = field_read_fields(array(
      'type' => 'unisender_lists_subscription',
      'entity_type' => $entity_type,
    )
  );
  foreach ($fields as $field) {
    $info = field_info_field($field['field_name']);
    if (isset($info['bundles'][$entity_type])) {
      $bundle = reset($info['bundles'][$entity_type]);
      $instance_info = field_info_instance($entity_type, $field['field_name'], $bundle);
      $field_options[$field['field_name']] = t("@label (@fieldname)",
        array(
          '@label' => $instance_info['label'],
          '@fieldname' => $field['field_name'],
        ));
    }
  }
  $form['unisender_field'] = array(
    '#type' => 'select',
    '#title' => t('REQUIRED: Unisender List Field(s)'),
    '#options' => $field_options,
    '#multiple' => TRUE,
    '#description' => t('Select the Unisender fields for which to allow segmentation.'),
    '#default_value' => $options['unisender_field'] ?? NULL,
    '#states' => array(
      'required' => array(
        ':input[name="options[vbo_operations][action::unisender_lists_add_to_list_action][selected]"]' => array('checked' => TRUE),
      ),
    ),
  );
  return $form;
}

function unisender_lists_cache_clear() {
  unisender_get_lists(TRUE);
  drupal_goto(drupal_get_destination());
}