<?php
/**
 * @file
 * Rules integration for the unisender lists module.
 */

/**
 * Implements hook_rules_action_info().
 */
function unisender_lists_rules_action_info() {
  $items = array();
  $items['unisender_lists_user_subscribe'] = array(
    'label' => t('Subscribe or unsubscribe entity from a Unisender list'),
    'parameter' => array(
      'entity' => array(
        'type' => 'entity',
        'label' => t('Entity'),
        'description' => t('The entity to subscribe/unsubscribe'),
      ),
      'field' => array(
        'type' => '*',
        'label' => t('Unisender List Subscription Field'),
        'description' => t('Subscription Field connected to the desired Unisender List.'),
        'restriction' => 'selector',
        'wrapped' => TRUE,
        'allow null' => FALSE,
      ),
      'subscribe' => array(
        'type' => 'boolean',
        'label' => t('Subscribe'),
        'description' => t('True to subscribe, False to unsubscribe'),
      ),
    ),
    'group' => t('Unisender'),
    'access callback' => 'unisender_lists_rules_access_callback',
    'base' => 'unisender_lists_rules_action_entity_subscribe',
  );
  return $items;
}

/**
 * Action callback: Subscribe an entity to a list.
 */
function unisender_lists_rules_action_entity_subscribe($entity, $field, $subscribe) {
  $field_info = $field->info();
  $choices = reset($field_info['instance']['default_value']);
  $choices['subscribe'] = $subscribe;
  unisender_lists_process_subscribe_form_choices($choices, $field_info['instance'], $field_info['field'], $entity->value());
}

/**
 * Access callback for the rules integration.
 */
function unisender_lists_rules_access_callback() {
  return user_access('administer unisender');
}
