<?php

/**
 * @file
 * Install, update and uninstall functions for the unisender module.
 */

/**
 * Implements hook_schema().
 */
function unisender_schema() {
  $schema['cache_unisender'] = drupal_get_schema_unprocessed('system', 'cache');
  $schema['cache_unisender']['description'] = 'Cache table for the Unisender module to store a list of subscribers member info.';

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function unisender_requirements($phase) {
  $requirements = array();
  // Ensure translations don't break at install time:
  $t = get_t();

  // Report Drupal version:
  if (in_array($phase, array('runtime', 'update'))) {
    // Check to see if Libraries module is being used.
    if (module_exists('libraries')) {
      $library = libraries_detect('unisender');
    }
    // If not we set the variable to false
    else {
      $library = FALSE;
    }
    $requirements['unisender'] = array(
      'title' => $t('Unisender'),
    );

    if ($library['installed']) {
      $requirements['unisender'] += array(
        'value' => $library['version'],
        'description' => $t('The Unisender wrapper library is installed correctly.'),
        'severity' => REQUIREMENT_OK,
      );
    }
    // No API from libraries or composer, punt!
    else {
      $requirements['unisender'] += array(
        'value' => $library ? $library['error'] : $t('The Unisender wrapper library is not installed'),
        'description' => $library ? $library['error message'] : '',
        'severity' => REQUIREMENT_ERROR,
      );
    }
  }

  return $requirements;
}
