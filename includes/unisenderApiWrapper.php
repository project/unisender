<?php

/**
 * Description of unisenderApiWrapper
 */
class unisenderApiWrapper extends unisenderApi {
  /**
   * @param string $name
   * @param array  $arguments
   *
   * @return string
   */
  public function __call($name, $arguments) {
    return $this->processApiResult(parent::__call($name, $arguments));
  }

  /**
   * @param array $params
   *
   * @return string
   */
  public function subscribe($params) {
    return $this->processApiResult(parent::subscribe($params));
  }


  /**
   * This method return information about contact.
   *
   * @param array $params Array: email, api_key
   *
   * @return array|false
   */
  public function getContact(array $params) {
    $result = parent::getContact($params);
    $result_decoded = json_decode($result, TRUE);

    return $result_decoded['result'] ?? $result_decoded;
  }

  protected function processApiResult($api_result) {
    if ($api_result) {
      $result_decoded = json_decode($api_result, TRUE);
      if (isset($result_decoded['error'])) {
        $result = $result_decoded;
      } else {
        $result = isset($result_decoded['result']) ? $result_decoded['result'] : array();
      }
    } else {
      $result = array('error' => t('Error occured while invoking API method @method', array('@method' => $name)));
    }
    return $result;
  }
}
